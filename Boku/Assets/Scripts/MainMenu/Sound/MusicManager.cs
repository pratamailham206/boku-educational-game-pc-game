﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    private static MusicManager _instance;

    public float BGMVolume;
    public float SFXVolume;

    public GameObject [] BGM;
    public GameObject [] SFX;

    void Start(){
        BGMVolume = 1;
        SFXVolume = 1;
    }

    public static MusicManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MusicManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if(SceneManager.GetActiveScene().name == "SettingScene"){
            BGMVolume = GameObject.Find("BGM").GetComponent<Slider>().value;
            SFXVolume = GameObject.Find("SFX").GetComponent<Slider>().value;
        }
        BGM = GameObject.FindGameObjectsWithTag("BGM");
        SFX = GameObject.FindGameObjectsWithTag("SFX");
        foreach(GameObject BGMAudio in BGM){
            BGMAudio.GetComponent<AudioSource>().volume = BGMVolume;
        }
        foreach(GameObject SFXAudio in SFX){
            SFXAudio.GetComponent<AudioSource>().volume = SFXVolume;
        }
    }
}
