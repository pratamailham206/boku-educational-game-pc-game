﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InsertUser : MonoBehaviour
{
    string URL = "https://unityprojectboku.000webhostapp.com/";
    public InputField inputUsername, inputEmail, inputPassword;
    public UserSelectScript getUser;

    public Text warningText;

    public void Register()
    {
        if(inputUsername.text != "" && inputEmail.text != "" && inputPassword.text != "")
        {
            StartCoroutine(AddUser(inputUsername.text.ToString(), inputEmail.text.ToString(), inputPassword.text.ToString(), "1"));
        }

        else
        {
            warningText.GetComponent<Text>().text = "Gagal!!";
        }
    }

    IEnumerator AddUser(string username, string email, string password, string progress)
    {
        WWWForm form = new WWWForm();
        form.AddField("addUsername", username);
        form.AddField("addEmail", email);
        form.AddField("addPassword", password);
        form.AddField("addProgress", progress);

        WWW www = new WWW(URL, form);

        warningText.GetComponent<Text>().text = "Menunggu...";
        yield return new WaitForSeconds(2f);
        StartCoroutine(getUser.getData());
        warningText.GetComponent<Text>().text = "Daftar Berhasil";
    }
}
