﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AuthManager : MonoBehaviour
{
    public GameObject LoginTab;
    public GameObject RegisterTab;

    [Header("Login")]

    public InputField email;
    public InputField password;
    public Text warning;

    [Header("Register")]

    public InputField emailReg;
    public InputField passwordReg;
    public Text warningReg;

    public static bool isLogged;
    bool registerStatus;

    public LoadingScript loading;

    bool logError;
    string errorMessage;
    private void Start()
    {
        isLogged = false;
        registerStatus = false;
        logError = false;
    }

    private void Update()
    {
        if(isLogged)
        {            
            warning.GetComponent<Text>().text = "Login Succes";
            loading.LoadScene();
        }

        if(registerStatus)
        {
            warningReg.text = "Register Succes";
        }

        if(logError)
        {
            warningReg.text = "Error Login" + errorMessage;
        }
    }

    public void moveScene()
    {
        loading.LoadScene();
    }

    public void Register()
    {

    }

    public void Login()
    {

    }    
}
