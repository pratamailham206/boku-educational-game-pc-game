﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellSystemScript : MonoBehaviour
{
    public InventorySystem inventory;
    public Text jumlahMesin;
    public Text jumlahKaca;
    public Text jumlahBody;
    public Text jumlahKerangka;
    public Text jumlahBan;
    public Text jumlahMobil;

    public GameObject saleObject;
    public GameObject buttonObject;
    bool left;

    public Sprite leftImage;
    public Sprite rightImage;

    private AudioSource sellSFX;

    private void Start()
    {
        left = false;
        sellSFX = GameObject.Find("Sell").GetComponent<AudioSource>();
    }

    void Update()
    {
        jumlahMobil.GetComponent<Text>().text = inventory.jumlahMobil.ToString();
        jumlahMesin.GetComponent<Text>().text = inventory.jumlahMesin.ToString();
        jumlahKaca.GetComponent<Text>().text = inventory.jumlahKacaMob.ToString();
        jumlahBody.GetComponent<Text>().text = inventory.jumlahBody.ToString();
        jumlahBan.GetComponent<Text>().text = inventory.jumlahBan.ToString();
        jumlahKerangka.GetComponent<Text>().text = inventory.jumlahKerangka.ToString();
    }

    public void jualMesin()
    {        
        if(inventory.jumlahMesin > 0)
        {
            StartCoroutine(animation());
        }

        IEnumerator animation()
        {
            jumlahMesin.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahMesin -= 1;
            inventory.jumlahUang += 150;
            inventory.MoveMaterial("mesin(Clone)");
        }        
    }

    public void jualBan()
    {
        if (inventory.jumlahBan > 0)
        {
            StartCoroutine(animation());
        }
        IEnumerator animation()
        {
            jumlahBan.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahBan -= 1;
            inventory.jumlahUang += 150;
            inventory.MoveMaterial("ban(Clone)");
        }
    }

    public void jualKacaMobil()
    {
        if (inventory.jumlahKacaMob > 0)
        {
            StartCoroutine(animation());
        }
        IEnumerator animation()
        {
            jumlahKaca.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahKacaMob -= 1;
            inventory.jumlahUang += 150;
            inventory.MoveMaterial("kaca(Clone)");
        }

    }

    public void jualKerangka()
    {
        if (inventory.jumlahKerangka > 0)
        {
            StartCoroutine(animation());
        }
        IEnumerator animation()
        {
            jumlahKerangka.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahKerangka -= 1;
            inventory.jumlahUang += 150;
            inventory.MoveMaterial("kerangka(Clone)");
        }
    }

    public void jualBodyMobil()
    {
        if (inventory.jumlahBody > 0)
        {
            StartCoroutine(animation());
        }
        IEnumerator animation()
        {
            jumlahBody.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahBody -= 1;
            inventory.jumlahUang += 150;
            inventory.MoveMaterial("body(Clone)");
        }
    }

    public void jualMobil()
    {
        if (inventory.jumlahMobil > 0)
        {
            StartCoroutine(animation());
        }
        IEnumerator animation()
        {
            jumlahMobil.GetComponent<Animator>().SetTrigger("Dec");
            sellSFX.Play();
            yield return new WaitForSeconds(0.5f);
            inventory.jumlahMobil -= 1;
            inventory.jumlahUang += 2000;
            inventory.MoveMaterial("mobil(Clone)");
        }
    }

    public void toggleLeftRight()
    {
        if(!left)
        {
            left = true;
            saleObject.GetComponent<Animator>().SetTrigger("hidden");
            buttonObject.GetComponent<Animator>().SetTrigger("hidden");
            buttonObject.GetComponent<Image>().sprite = rightImage;
        }

        else if(left)
        {
            left = false;
            buttonObject.GetComponent<Animator>().SetTrigger("view");
            saleObject.GetComponent<Animator>().SetTrigger("view");
            buttonObject.GetComponent<Image>().sprite = leftImage;
        }
    }
}
