﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRobotProgram : MonoBehaviour
{
    public RectTransform canvas;
    public GameObject button;
    public Scrollbar scrollbar;

    float defaultViewX;
    float defaultViewY;

    GameObject[] listButton;
    int index;
    void Start()
    {
        index = 0;
        listButton = new GameObject[40];
        defaultViewX = canvas.GetComponent<RectTransform>().sizeDelta.x;
        defaultViewY = canvas.GetComponent<RectTransform>().sizeDelta.y;
    }

    public void AddFunction(string name)
    {
        if (index >= 16)
        {
            canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(canvas.sizeDelta.x, canvas.sizeDelta.y + 35f);
            decScrollbar();
        }
        listButton[index] = Instantiate(button, new Vector3(button.transform.position.x, button.transform.position.y - 27 * index, button.transform.position.z), Quaternion.identity) as GameObject;
        listButton[index].transform.SetParent(canvas.transform, false);
        listButton[index].GetComponent<ButtonMoveScript>().Name = name;
        listButton[index].GetComponent<ButtonMoveScript>().indexs = index;
        index += 1;
    }

    void decScrollbar()
    {
        scrollbar.GetComponent<Scrollbar>().value = 0;
    }

    public void DeleteFunction()
    {
        Destroy(listButton[index - 1]);
        index -= 1;
    }

    public void DeleteAllFunction()
    {
        for(int i = 0; i < index; i++)
        {
            Destroy(listButton[i]);           
        }
        index = 0;
        scrollbar.GetComponent<Scrollbar>().value = 1;
        scrollbar.GetComponent<Scrollbar>().size = 1;
        canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(defaultViewX, defaultViewY);
    }
}
