﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetToggle : MonoBehaviour
{
    public GameObject toggleCommand;

    public void setToggleCommand(){
        toggleCommand.GetComponent<ToggleCommandButton>().commandButton = this.gameObject;
        toggleCommand.GetComponent<ToggleCommandButton>().ToggleButton();
    }
}
