﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScreen : MonoBehaviour
{
    public GridBehavior playerHistory;

    public int star;
    public bool star1;
    public bool star2;

    public int star2Requirement;

    public bool star3;
    public int star3Requirement;

    public GameObject star1Obj;
    public GameObject star2Obj;
    public GameObject star3Obj;
    
    // Start is called before the first frame update
    void Start()
    {
        star1 = true;
        star = 1;

        star2 = false;
        star3 = false;

        star1Obj.SetActive(true);

        if(playerHistory.actionExecuted <= star2Requirement){
            star2 = true;
            star++;
            star2Obj.SetActive(true);
        }
            

        if(playerHistory.index <= star3Requirement){
            star3 = true;
            star++;
            star3Obj.SetActive(true);
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("LevelSelection");
    }
}
