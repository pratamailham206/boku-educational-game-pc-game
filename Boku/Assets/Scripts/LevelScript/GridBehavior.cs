﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridBehavior : MonoBehaviour
{
    //GRID PROGRAM
    [Header("Grid System")]
    public int rows;
    public int columns;
    public float scale = 1;

    public GameObject gridObject;
    public Vector3 leftBottomLocation = new Vector3(0, 0, 0);

    public GameObject[,] gridArray;

    public GameObject[] executeButton;
    public GameObject stopButton;


    //ROBOT PROGRAM
    [Header("Robot")]
    public GameObject objectToMove;
    List<System.Action> unitQueue = new List<System.Action>();
    public int index;
    public bool moving;

    public int startX = 2;
    public int startY = 2;

    int initialStartX;
    int initialStartY;

    public float speed = 5;

    //UI PROGRAM
    [Header("UI Robot")]
    public GameObject UIRobot;

    //SHOP
    [Header("Shop Program")]
    public GameObject ShopObject;
    public int shopXpos;
    public int shopYpos;

    //UI SHOP
    [Header("UI Shop")]
    public GameObject inventory;

    //STATION
    [Header("Station Program")]
    public GameObject[] listStation;

    //IF WHILE PROGRAM
    public bool isMoving;

    public ConditionTab condition;
    public bool ifConditionMet;

    public int actionExecuted;
    public bool objectObstruction;

    int indexLoop;

    private AudioSource BuySFX;
    private AudioSource CraftSFX;

    [Header("Particle and Animation")]
    public GameObject buyParticle;
    public GameObject electricParticle;
    public GameObject hitParticle;
    public GameObject starParticle;
    public GameObject rockParticle;
    public GameObject debrisParticle;
    public GameObject glassParticle;
    public GameObject assemblyParticle;
    public GameObject FloatingTextPrefab;

    void Start()
    {
        index = 0;
        actionExecuted = 0;
        moving = false;
        isMoving = false;

        ifConditionMet = true;

        BuySFX = GameObject.Find("Buy").GetComponent<AudioSource>();
        CraftSFX = GameObject.Find("Craft").GetComponent<AudioSource>();

        //INITIALIZE ROBOT
        objectToMove.transform.position = new Vector3(gridArray[startX, startY].transform.position.x, gridArray[startX, startY].transform.position.y + 0.35f, gridArray[startX, startY].transform.position.z);

        //SET TEMP ROBOT POSITION
        initialStartX = startX;
        initialStartY = startY;

        //INITIALIZE SHOP
        ShopObject.GetComponent<ShopBehavior>().Xpos = shopXpos;
        ShopObject.GetComponent<ShopBehavior>().Ypos = shopYpos;

        ShopObject.transform.position = new Vector3(gridArray[ShopObject.GetComponent<ShopBehavior>().Xpos, ShopObject.GetComponent<ShopBehavior>().Ypos].transform.position.x, gridArray[ShopObject.GetComponent<ShopBehavior>().Xpos, ShopObject.GetComponent<ShopBehavior>().Ypos].transform.position.y + 0.10f, gridArray[ShopObject.GetComponent<ShopBehavior>().Xpos, ShopObject.GetComponent<ShopBehavior>().Ypos].transform.position.z);
        ShopObject.transform.Rotate(0f, 180f, 0f);
        ShopObject.transform.localScale = new Vector3(ShopObject.transform.localScale.x * scale / 1.2f, ShopObject.transform.localScale.y * scale / 1.2f, ShopObject.transform.localScale.z * scale / 1.2f);

        //INITIALIZE STATION
        for(int i = 0; i < listStation.Length; i++)
        {
            listStation[i].transform.position = new Vector3(gridArray[listStation[i].GetComponent<StationsScript>().Xpos, listStation[i].GetComponent<StationsScript>().Ypos].transform.position.x, gridArray[listStation[i].GetComponent<StationsScript>().Xpos, listStation[i].GetComponent<StationsScript>().Ypos].transform.position.y + 0.10f, gridArray[listStation[i].GetComponent<StationsScript>().Xpos, listStation[i].GetComponent<StationsScript>().Ypos].transform.position.z);
            listStation[i].transform.localScale = new Vector3(listStation[i].transform.localScale.x * scale / 1.2f, listStation[i].transform.localScale.y * scale / 1.2f, listStation[i].transform.localScale.z * scale / 1.2f);
        }

        stopButton.GetComponent<Button>().interactable = false;
    }

    private void Awake()
    {
        gridArray = new GameObject[columns, rows];

        if(gridObject)
        {
            GenerateGrid();
        }
        else
        {
            Debug.Log("Missing Prefab");
        }        
    }

    void Update()
    {
        if (moving)
        {
            for(int i= 0; i < executeButton.Length; i++)
            {
                executeButton[i].GetComponent<Button>().interactable = false;
                stopButton.GetComponent<Button>().interactable = true;
            }

            if (index == unitQueue.Count)
            {
                moving = false;
                isMoving = false;
            }
            else
            {
                unitQueue[index]();
            }
            
        }

        else
        {
            for (int i = 0; i < executeButton.Length; i++)
            {
                executeButton[i].GetComponent<Button>().interactable = true;
                stopButton.GetComponent<Button>().interactable = false;
            }
        }
    }    

    //INITIALIZE GRID
    void GenerateGrid()
    {
        for(int i = 0; i < columns; i++)
        {
            for(int j = 0; j < rows; j++)
            {
                GameObject obj = Instantiate(gridObject, new Vector3(leftBottomLocation.x + scale * i, leftBottomLocation.y, leftBottomLocation.z + scale * j), Quaternion.identity);
                obj.transform.localScale = new Vector3(obj.transform.localScale.x * scale / 1.2f, obj.transform.localScale.y, obj.transform.localScale.z * scale / 1.2f);
                obj.transform.SetParent(gameObject.transform);
                obj.GetComponent<Grids>().x = i;
                obj.GetComponent<Grids>().y = j;
                gridArray[i, j] = obj;
            }
        }
    }

    //ROBOT FUNCTION
    void MoveLeft()
    { 
        if(moving)
        {
            objectObstruction = true;
            for(int i = 0; i < listStation.Length; i++){
                if (startX - 1 == listStation[i].GetComponent<StationsScript>().Xpos && startY == listStation[i].GetComponent<StationsScript>().Ypos)
                    objectObstruction = false;
            }
            if (startX - 1 == shopXpos && startY == shopYpos)
                objectObstruction = false;


            if (startX > 0 && ifConditionMet && objectObstruction)
            { // checking grid and if condition
                objectToMove.transform.LookAt(new Vector3(gridArray[startX - 1, startY].transform.position.x, gridArray[startX - 1, startY].transform.position.y + 0.35f, gridArray[startX - 1, startY].transform.position.z));
                objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, new Vector3(gridArray[startX - 1, startY].transform.position.x, gridArray[startX - 1, startY].transform.position.y + 0.35f, gridArray[startX - 1, startY].transform.position.z), speed * Time.deltaTime);
                isMoving = false; // toggle object currently moving
                if (Vector3.Distance(objectToMove.transform.position, new Vector3(gridArray[startX - 1, startY].transform.position.x, gridArray[startX - 1, startY].transform.position.y + 0.35f, gridArray[startX - 1, startY].transform.position.z)) <= 0f)
                {
                    isMoving = true; // object stopped moving
                    startX = startX - 1;
                }
            }
            if (isMoving)
            { // will not enter while object is still moving
                index += 1; // index will still move even if movement is invalid
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
            return;
        }
    }
    void MoveRight()
    {
        if(moving)
        {
            objectObstruction = true;
            for(int i = 0; i < listStation.Length; i++){
                if (startX + 1 == listStation[i].GetComponent<StationsScript>().Xpos && startY == listStation[i].GetComponent<StationsScript>().Ypos)
                    objectObstruction = false;
            }
            if (startX + 1 == shopXpos && startY == shopYpos)
                objectObstruction = false;

            if (startX < columns - 1 && ifConditionMet && objectObstruction)
            {
                objectToMove.transform.LookAt(new Vector3(gridArray[startX + 1, startY].transform.position.x, gridArray[startX + 1, startY].transform.position.y + 0.35f, gridArray[startX + 1, startY].transform.position.z));
                objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, new Vector3(gridArray[startX + 1, startY].transform.position.x, gridArray[startX + 1, startY].transform.position.y + 0.35f, gridArray[startX + 1, startY].transform.position.z), speed * Time.deltaTime);
                isMoving = false;
                if (Vector3.Distance(objectToMove.transform.position, new Vector3(gridArray[startX + 1, startY].transform.position.x, gridArray[startX + 1, startY].transform.position.y + 0.35f, gridArray[startX + 1, startY].transform.position.z)) <= 0f)
                {
                    isMoving = true;
                    startX = startX + 1;
                }
            }
            if (isMoving)
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
            return;
        }
    }
    void MoveUp()
    {
        if (moving)
        {
            objectObstruction = true;
            for(int i = 0; i < listStation.Length; i++){
                if (startY + 1 == listStation[i].GetComponent<StationsScript>().Ypos && startX == listStation[i].GetComponent<StationsScript>().Xpos)
                    objectObstruction = false;
            }
            if (startY + 1 == shopYpos && startX == shopXpos)
                objectObstruction = false;

            if (startY < rows - 1 && ifConditionMet && objectObstruction)
            {
                objectToMove.transform.LookAt(new Vector3(gridArray[startX, startY + 1].transform.position.x, gridArray[startX, startY + 1].transform.position.y + 0.35f, gridArray[startX, startY + 1].transform.position.z));
                objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, new Vector3(gridArray[startX, startY + 1].transform.position.x, gridArray[startX, startY + 1].transform.position.y + 0.35f, gridArray[startX, startY + 1].transform.position.z), speed * Time.deltaTime);
                isMoving = false;
                if (Vector3.Distance(objectToMove.transform.position, new Vector3(gridArray[startX, startY + 1].transform.position.x, gridArray[startX, startY + 1].transform.position.y + 0.35f, gridArray[startX, startY + 1].transform.position.z)) <= 0f)
                {
                    isMoving = true;
                    startY = startY + 1;
                }
            }
            if (isMoving)
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }

            return;

        }
    }
    void MoveDown()
    {
        if(moving)
        {
            objectObstruction = true;
            for(int i = 0; i < listStation.Length; i++){
                if (startY - 1 == listStation[i].GetComponent<StationsScript>().Ypos && startX == listStation[i].GetComponent<StationsScript>().Xpos)
                    objectObstruction = false;
            }
            if (startY - 1 == shopYpos && startX == shopXpos)
                objectObstruction = false;

            if (startY > 0 && ifConditionMet && objectObstruction)
            {
                objectToMove.transform.LookAt(new Vector3(gridArray[startX, startY - 1].transform.position.x, gridArray[startX, startY - 1].transform.position.y + 0.35f, gridArray[startX, startY - 1].transform.position.z));
                objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, new Vector3(gridArray[startX, startY - 1].transform.position.x, gridArray[startX, startY - 1].transform.position.y + 0.35f, gridArray[startX, startY - 1].transform.position.z), speed * Time.deltaTime);
                isMoving = false;
                if (Vector3.Distance(objectToMove.transform.position, new Vector3(gridArray[startX, startY - 1].transform.position.x, gridArray[startX, startY - 1].transform.position.y + 0.35f, gridArray[startX, startY - 1].transform.position.z)) <= 0f)
                {
                    isMoving = true;
                    startY = startY - 1;
                }
            }
            if (isMoving)
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }

            return;

        }
    }
    void StartIf()
    {
        index += 1;
        isMoving = true;
        return;
    }
    void EndIf()
    {
        ifConditionMet = true;
        index += 1;
        return;
    }
    void StartLoop()
    {
        indexLoop = index;
        index += 1;
        return;
    }
    void EndLoop()
    {
        if (condition.ifConditionFulfilled)
        {
            index = indexLoop;
        }
        else
        {
            ifConditionMet = true;
            index += 1;
        }
        return;
    }

    public void AddStartIf()
    {
        unitQueue.Add(() => StartIf());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Start If");
    }

    public void AddEndIf()
    {
        unitQueue.Add(() => EndIf());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("End If");
    }

    public void AddStartLoop()
    {
        unitQueue.Add(() => StartLoop());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Start Loop");
    }

    public void AddEndLoop()
    {
        unitQueue.Add(() => EndLoop());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("End Loop");
    }

    public void AddCheckMoney()
    {
        unitQueue.Add(() => condition.ifUangTrue());
    }

    public void AddCheckBesi()
    {
        unitQueue.Add(() => condition.ifBesiTrue());
    }

    public void AddCheckKaret()
    {
        unitQueue.Add(() => condition.ifKaretTrue());
    }

    public void AddCheckKaca()
    {
        unitQueue.Add(() => condition.ifKacaTrue());
    }

    public void AddCheckPlastik()
    {
        unitQueue.Add(() => condition.ifPlastikTrue());
    }
    
    void BeliBesi()
    {
        if (moving)
        {
            int xPos = ShopObject.GetComponent<ShopBehavior>().Xpos;
            int yPos = ShopObject.GetComponent<ShopBehavior>().Ypos;
            int uang = inventory.GetComponent<InventorySystem>().jumlahUang;

            if ((xPos - 1 == startX && yPos == startY && uang >= 100|| xPos + 1 == startX && yPos == startY && uang >= 100 || xPos == startX && yPos - 1 == startY && uang >= 100 || xPos == startX && yPos + 1 == startY && uang >= 100) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanBesi();
                StartCoroutine(spawnBuyParticle());
                return;
            }

            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }
    void BeliKaca()
    {
        if (moving)
        {
            int xPos = ShopObject.GetComponent<ShopBehavior>().Xpos;
            int yPos = ShopObject.GetComponent<ShopBehavior>().Ypos;
            int uang = inventory.GetComponent<InventorySystem>().jumlahUang;

            if ((xPos - 1 == startX && yPos == startY && uang >= 100 || xPos + 1 == startX && yPos == startY && uang >= 100 || xPos == startX && yPos - 1 == startY && uang >= 100 || xPos == startX && yPos + 1 == startY && uang >= 100) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanKaca();
                StartCoroutine(spawnBuyParticle());
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }
    void BeliKaret()
    {
        if (moving)
        {
            int xPos = ShopObject.GetComponent<ShopBehavior>().Xpos;
            int yPos = ShopObject.GetComponent<ShopBehavior>().Ypos;
            int uang = inventory.GetComponent<InventorySystem>().jumlahUang;

            if ((xPos - 1 == startX && yPos == startY && uang >= 100 || xPos + 1 == startX && yPos == startY && uang >= 100 || xPos == startX && yPos - 1 == startY && uang >= 100 || xPos == startX && yPos + 1 == startY && uang >= 100) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanKaret();
                StartCoroutine(spawnBuyParticle());
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void BeliPlastik()
    {
        if (moving)
        {
            int xPos = ShopObject.GetComponent<ShopBehavior>().Xpos;
            int yPos = ShopObject.GetComponent<ShopBehavior>().Ypos;
            int uang = inventory.GetComponent<InventorySystem>().jumlahUang;

            if ((xPos - 1 == startX && yPos == startY && uang >= 100 || xPos + 1 == startX && yPos == startY && uang >= 100 || xPos == startX && yPos - 1 == startY && uang >= 100 || xPos == startX && yPos + 1 == startY && uang >= 100) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanPlastik();
                StartCoroutine(spawnBuyParticle());
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    IEnumerator spawnBuyParticle()
    {
        BuySFX.Play();
        Instantiate(buyParticle, new Vector3(ShopObject.transform.position.x, ShopObject.transform.position.y + 1.5f, ShopObject.transform.position.z), Quaternion.identity);
        if (FloatingTextPrefab)
        {
            showFloatingText();
        }
        moving = false;
        yield return new WaitForSeconds(0.5f);
        index += 1;
        if(ifConditionMet){
            actionExecuted++;
        }
        moving = true;
    }
    public void showFloatingText()
    {
        FloatingTextPrefab.GetComponent<TextMesh>().text = "+1!!";
        Instantiate(FloatingTextPrefab, ShopObject.GetComponent<Transform>().position, Quaternion.identity, transform);
    }

    IEnumerator operateStationAnimation(GameObject particles, GameObject station)
    {
        Instantiate(particles, new Vector3(station.transform.position.x, station.transform.position.y + 1.5f, station.transform.position.z), Quaternion.identity);
        Instantiate(particles, new Vector3(station.transform.position.x, station.transform.position.y + 1.5f, station.transform.position.z), Quaternion.identity);
        if (FloatingTextPrefab)
        {
            showFloatingTextOnStation(station);
        }
        moving = false;
        yield return new WaitForSeconds(1f);
        if(ifConditionMet){
            actionExecuted++;
        }
        index += 1;
        moving = true;
    }

    public void showFloatingTextOnStation(GameObject stations)
    {
        FloatingTextPrefab.GetComponent<TextMesh>().text = "Berhasil!!";
        FloatingTextPrefab.GetComponent<TextMesh>().fontSize = 54;      
        Instantiate(FloatingTextPrefab, stations.GetComponent<Transform>().position, Quaternion.identity, transform);
    }

    //PRODUCT
    void operateStationMesin()
    {
        if (moving)
        {
            int xPos = listStation[0].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[0].GetComponent<StationsScript>().Ypos;
            int besi = inventory.GetComponent<InventorySystem>().jumlahBesi;

            if ((xPos - 1 == startX && yPos == startY && besi > 0 || xPos + 1 == startX && yPos == startY && besi > 0 || xPos == startX && yPos - 1 == startY && besi > 0 || xPos == startX && yPos + 1 == startY && besi > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanMesin();
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(electricParticle, listStation[0]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void operateStationBan()
    {
        if (moving)
        {
            int xPos = listStation[1].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[1].GetComponent<StationsScript>().Ypos;
            int karet = inventory.GetComponent<InventorySystem>().jumlahKaret;

            if ((xPos - 1 == startX && yPos == startY && karet > 0 || xPos + 1 == startX && yPos == startY && karet > 0 || xPos == startX && yPos - 1 == startY && karet > 0 || xPos == startX && yPos + 1 == startY && karet > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanBan();
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(rockParticle, listStation[1]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void operateStationKacaMob()
    {
        if (moving)
        {
            int xPos = listStation[2].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[2].GetComponent<StationsScript>().Ypos;
            int kaca = inventory.GetComponent<InventorySystem>().jumlahKaca;

            if ((xPos - 1 == startX && yPos == startY && kaca > 0 || xPos + 1 == startX && yPos == startY && kaca > 0 || xPos == startX && yPos - 1 == startY && kaca > 0 || xPos == startX && yPos + 1 == startY && kaca > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanKacaMob();
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(glassParticle, listStation[2]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void operateStationKerangka()
    {
        if (moving)
        {
            int xPos = listStation[3].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[3].GetComponent<StationsScript>().Ypos;
            int besi = inventory.GetComponent<InventorySystem>().jumlahBesi;

            if ((xPos - 1 == startX && yPos == startY && besi > 0 || xPos + 1 == startX && yPos == startY && besi > 0 || xPos == startX && yPos - 1 == startY && besi > 0 || xPos == startX && yPos + 1 == startY && besi > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanKerangka();
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(debrisParticle, listStation[3]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void operateStationBody()
    {
        if (moving)
        {
            int xPos = listStation[4].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[4].GetComponent<StationsScript>().Ypos;
            int plastik = inventory.GetComponent<InventorySystem>().jumlahPlastik;

            if ((xPos - 1 == startX && yPos == startY && plastik > 0 || xPos + 1 == startX && yPos == startY && plastik > 0 || xPos == startX && yPos - 1 == startY && plastik > 0 || xPos == startX && yPos + 1 == startY && plastik > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanBody();
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(starParticle, listStation[4]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    void operateStationAssembly()
    {
        if (moving)
        {
            int xPos = listStation[5].GetComponent<StationsScript>().Xpos;
            int yPos = listStation[5].GetComponent<StationsScript>().Ypos;

            int mesin = inventory.GetComponent<InventorySystem>().jumlahMesin;
            int ban = inventory.GetComponent<InventorySystem>().jumlahBan;
            int kaca = inventory.GetComponent<InventorySystem>().jumlahKacaMob;
            int body = inventory.GetComponent<InventorySystem>().jumlahBody;
            int rangka = inventory.GetComponent<InventorySystem>().jumlahKerangka;

            if ((xPos - 1 == startX && yPos == startY && mesin > 0 && ban > 0 && kaca > 0 && body > 0 && rangka > 0 || xPos + 1 == startX && yPos == startY && mesin > 0 && ban > 0 && kaca > 0 && body > 0 && rangka > 0 || xPos == startX && yPos - 1 == startY && mesin > 0 && ban > 0 && kaca > 0 && body > 0 && rangka > 0 || xPos == startX && yPos + 1 == startY && mesin > 0 && ban > 0 && kaca > 0 && body > 0 && rangka > 0) && ifConditionMet)
            {
                inventory.GetComponent<InventorySystem>().simpanMobil();               
                CraftSFX.Play();
                StartCoroutine(operateStationAnimation(assemblyParticle, listStation[5]));
                return;
            }
            else
            {
                index += 1;
                if(ifConditionMet){
                    actionExecuted++;
                }
            }
        }
    }

    //ADD FUNCTION TO PROGRAM
    public void AddMoveLeft()
    {
        unitQueue.Add(() =>MoveLeft());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Ke Kiri");
    }
    public void AddMoveRight()
    {
        unitQueue.Add(() => MoveRight());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Ke Kanan");
    }
    public void AddMoveUp()
    {
        unitQueue.Add(() => MoveUp());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Ke Atas");
    }
    public void AddMoveDown()
    {
        unitQueue.Add(() => MoveDown());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Ke Bawah");
    }

    //RESOURCE
    public void AddBeliBesi()
    {
        unitQueue.Add(() => BeliBesi());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Beli Besi");        
    }
    public void AddBeliKaca()
    {
        unitQueue.Add(() => BeliKaca());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Beli Kaca");
    }
    public void AddBeliKaret()
    {
        unitQueue.Add(() => BeliKaret());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Beli Karet");
    }
    public void AddBeliPlastik()
    {
        unitQueue.Add(() => BeliPlastik());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Beli Plastik");
    }

    //OPERATE STATION
    public void AddOperateStationMesin()
    {
        unitQueue.Add(() => operateStationMesin());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Mesin");
    }
    public void AddOperateStationBan()
    {
        unitQueue.Add(() => operateStationBan());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Roda");
    }
    public void AddOperateStationKacaMob()
    {
        unitQueue.Add(() => operateStationKacaMob());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Kaca Mobil");
    }

    public void AddOperateStationKerangka()
    {
        unitQueue.Add(() => operateStationKerangka());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Kerangka Mobil");
    }

    public void AddOperateStationBody()
    {
        unitQueue.Add(() => operateStationBody());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Body Mobil");
    }

    public void AddOperateStationAssembly()
    {
        unitQueue.Add(() => operateStationAssembly());
        UIRobot.GetComponent<UIRobotProgram>().AddFunction("Buat Mobil");
    }

    //OPERATION FUNCTION
    public void ExecuteFunction()
    {
        index = 0;
        moving = true;
        isMoving = true;
    }

    public void StopFunction()
    {
        moving = false;
        isMoving = false;
        index = 0;
    }

    public void RestartFunction()
    {
        startX = initialStartX;
        startY = initialStartY;

        index = 0;
        moving = false;
        actionExecuted = 0;
        
        objectToMove.transform.position = new Vector3(gridArray[startX, startY].transform.position.x, gridArray[startX, startY].transform.position.y + 0.35f, gridArray[startX, startY].transform.position.z);
    }
    public void DeleteFunction()
    {
        unitQueue.RemoveAt(unitQueue.Count - 1);
        index -= 1;
        UIRobot.GetComponent<UIRobotProgram>().DeleteFunction();
        if(index < 0)
        {
            index = 0;
            Debug.Log("index 0");
        }
    }
    public void DeleteAll()
    {
        for(int i = unitQueue.Count - 1; i >= 0; i--)
        {
            unitQueue.RemoveAt(i);            
        }
        UIRobot.GetComponent<UIRobotProgram>().DeleteAllFunction();
        index = 0;
    }  
}
