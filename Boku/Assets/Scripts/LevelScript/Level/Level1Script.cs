﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level1Script : MonoBehaviour
{
    public int star;

    public GridBehavior level;

    public GameObject UIAction;
    public GameObject UITask;
    public GameObject UIInventory;
    public GameObject UIToggleButton;
    public GameObject WinScreen;

    public GameObject [] starUI;

    public int score;
    public Text scoreText;

    public PauseHandlerScript timer;
    

    private bool levelComplete;
    private int starRemoved;


    void Start()
    {
        levelComplete = false;
        starRemoved = 3;
        GetComponent<UserSelectScript>();
    }

    public void level1Cleared()
    {
        if (UserSelectScript.levelCleared[1] == false)
        {
            UserSelectScript.levelComplete += 1;
            UserSelectScript.levelCleared[1] = true;

        }
        if (UserSelectScript.starAquired[0] < star)
        {
            UserSelectScript.star += star - UserSelectScript.starAquired[0];
            UserSelectScript.starAquired[0] = star;
            UserSelectScript.score += score;
        }
        SceneManager.LoadScene("LevelSelection");
        Debug.Log(UserSelectScript.score);
    }

    void Update()
    {
        if(!levelComplete){
            if(TaskFulfilled()){
                levelComplete = true;
                level.moving = false;
                StartCoroutine(winScreenAnimation());
            }
        }
    }

    IEnumerator winScreenAnimation()
    {
        yield return new WaitForSeconds(1f);
        UIAction.SetActive(false);
        UITask.SetActive(false);
        UIInventory.SetActive(false);
        UIToggleButton.SetActive(false);
        WinScreen.SetActive(true);
        WinScreen.GetComponent<Animator>().SetTrigger("view");
        yield return new WaitForSeconds(0.5f);
        while (starRemoved > star)
        {
            starRemoved--;
            starUI[starRemoved].SetActive(false);
        }
    }

    bool TaskFulfilled(){
        if(level.startX == 4 && level.startY == 3){
            star++;
            score = 1000 - Mathf.FloorToInt(timer.timeRemaining) - level.actionExecuted * 10;
            if (score < 0){
                score = 0;
            }
            scoreText.text = score.ToString();

            if(score >= 850){
                star++;
            }
            if(score >= 900){
                star++;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
