﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButtonGrid : MonoBehaviour
{
    public GridBehavior roboGrid;

    public GameObject NextButton;

    public int DestinationWantedX;
    public int DestinationWantedY;



    void Start(){
        NextButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(roboGrid.startX == DestinationWantedX && roboGrid.startY == DestinationWantedY){
            NextButton.SetActive(true);
        }
    }
}
