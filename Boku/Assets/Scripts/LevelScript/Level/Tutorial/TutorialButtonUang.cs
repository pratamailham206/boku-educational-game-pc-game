﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButtonUang : MonoBehaviour
{
    public InventorySystem roboInventory;

    public GameObject NextButton;

    public int TargetUang;
    
    void Start(){
        NextButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(roboInventory.jumlahUang >= TargetUang){
            NextButton.SetActive(true);
        }
    }
}
