﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTutorial : MonoBehaviour
{
    public GameObject [] tutorialUI;
    public GameObject tutorialUIAll;
    public GameObject transparentLayer;

    private int currentTutorialPanel;


    // Start is called before the first frame update
    void Start()
    {
        currentTutorialPanel = 0;
    }

    public void moveToNextPanel(){
        
        currentTutorialPanel++;
        if(currentTutorialPanel < tutorialUI.Length){
            tutorialUI[currentTutorialPanel].SetActive(true);
            tutorialUI[currentTutorialPanel-1].SetActive(false);
        }
        else{
            tutorialUIAll.SetActive(false);
        }
    }

    public void toggleTransparent(){
        if(transparentLayer.activeSelf){
            transparentLayer.SetActive(false);
        }
        else{
            transparentLayer.SetActive(true);
        }
        
    }
}
