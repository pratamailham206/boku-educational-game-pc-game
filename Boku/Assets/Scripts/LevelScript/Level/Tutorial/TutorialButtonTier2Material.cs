﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButtonTier2Material : MonoBehaviour
{
    public InventorySystem roboInventory;

    public GameObject NextButton;

    public int TargetMesin;
    public int TargetKacaMob;
    public int TargetBan;
    public int TargetKerangka;
    public int TargetBody;
    
    void Start(){
        NextButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(roboInventory.jumlahMesin >= TargetMesin && roboInventory.jumlahKacaMob == TargetKacaMob && roboInventory.jumlahBan == TargetBan && roboInventory.jumlahKerangka >= TargetKerangka && roboInventory.jumlahBody >= TargetBody){
            NextButton.SetActive(true);
        }
    }
}
