﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level4Script : MonoBehaviour
{
    public int star;

    public GridBehavior level;
    public InventorySystem playerInventory;

    public GameObject UIAction;
    public GameObject UITask;
    public GameObject UIInventory;
    public GameObject UISellSystem;
    public GameObject UIToggleButton;

    public GameObject WinScreen;

    public GameObject [] starUI;

    public PauseHandlerScript timer;

    public int score;
    public Text scoreText;

    private bool levelComplete;
    private int starRemoved;

    void Start()
    {
        levelComplete = false;
        starRemoved = 3;
        GetComponent<UserSelectScript>();
    }

    public void level4Cleared()
    {
        if(UserSelectScript.levelCleared[4] == false){
            UserSelectScript.levelComplete += 1;
            UserSelectScript.levelCleared[4] = true;
        }
        if(UserSelectScript.starAquired[3] < star){
            UserSelectScript.star += star - UserSelectScript.starAquired[3];
            UserSelectScript.starAquired[3] = star;
            UserSelectScript.score += score;
        }
        SceneManager.LoadScene("LevelSelection");
    }

    void Update()
    {
        if(!levelComplete){
            if(TaskFulfilled()){
                levelComplete = true;
                level.moving = false;
                StartCoroutine(winScreenAnimation());
            }
        }
    }

    IEnumerator winScreenAnimation()
    {
        yield return new WaitForSeconds(1f);
        UIAction.SetActive(false);
        UITask.SetActive(false);
        UIInventory.SetActive(false);
        UISellSystem.SetActive(false);
        UIToggleButton.SetActive(false);

        WinScreen.SetActive(true);
        WinScreen.GetComponent<Animator>().SetTrigger("view");
        yield return new WaitForSeconds(0.5f);
        while (starRemoved > star)
        {
            starRemoved--;
            starUI[starRemoved].SetActive(false);
        }
    }

    bool TaskFulfilled(){
        if(playerInventory.jumlahMobil >= 1){
            star++;
            score = 1000 - Mathf.FloorToInt(timer.timeRemaining) - level.actionExecuted * 5;
            if (score < 0){
                score = 0;
            }
            scoreText.text = score.ToString();

            if(score >= 650){
                star++;
            }
            if(score >= 775){
                star++;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
