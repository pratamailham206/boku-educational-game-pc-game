﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public Text starText;
    public Text scoreText;
    void Start()
    {
        GetComponent<UserSelectScript>();
    }

    // Update is called once per frame
    void Update()
    {
        starText.GetComponent<Text>().text = UserSelectScript.star.ToString();
        scoreText.GetComponent<Text>().text = UserSelectScript.score.ToString();
    }
}
