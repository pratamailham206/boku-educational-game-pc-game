﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class GameProgress
{
    public int levelComplete;
    public int star;
    public int score;
    public bool[] levelCleared;
    public int[] starAquired;
}