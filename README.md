# Boku - Educational Game - PC Game


## About

Boku adalah sebuah video game edukasi dengan genre puzzle, strategy, simulation. Memiliki konsep dimana kita mengatur proses produksi dalam sebuah pabrik mobil sesuai dengan task atau permintaan yang ada pada setiap level. Player diharuskan untuk membuat dan mengelola resource yang dibutuhkan, mengalokasikan dana untuk kebutuhan produksi, membuat material untuk mobil, dan memprogram robot sehingga proses produksi material dan mobil dapat dilakukan. Terdapat 6 level yang harus diselesaikan oleh player, dimana setiap level memiliki kesulitan dan trik tersendiri untuk diselesaikan. Semakin tinggi level maka kesulitan juga akan semakin meningkat, ditambah dengan fitur dan mechanic baru yang akan diperkenalkan di beberapa level akan membuat player semakin bervariasi untuk menyelesaikan task yang ada pada setiap level.

## Genre

Puzzle, Strategy, Simulation

## Platform

Windows

## Download

https://ilhampratama.itch.io/boku

## Gameplay

Dalam permainan Boku player bertugas untuk menyelesaikan semua level yang berjumlah 6. Setiap level, memiliki task atau goal yang harus diselesaikan agar level selanjutnya dapat terbuka. Setiap task biasanya berisi bagaimana player diharuskan untuk membuat suatu material atau diharuskan untuk memproduksi sesuatu yang berkaitan dengan mobil. Kemudian player akan disediakan sebuah robot, dan player diharuskan untuk memprogram dan menggerakkan robot tersebut agar dapat memproses dan memproduksi material yang ditentukan pada task level tersebut. Terdapat beberapa perintah seperti move, buy, dan create yang dapat digunakan untuk membuat robot bergerak, dan memproses barang-barang yang digunakan untuk membuat material. Serta if dan while yang digunakan untuk melakukan looping dan beberapa kondisi if. Dalam setiap level terdapat beberapa station yang digunakan untuk memproses dan membuat material, serta terdapat pasar bahan yang digunakan untuk membeli beberapa resource yang digunakan untuk membuat bahan. Setelah player menyelesaikan level, player akan mendapatkan star dan exp sebagai reward, dan level selanjutnya akan terbuka.


## Development Team
- 4210181002 Farhan Muhammad

- 4210181012 Hosea Nathaniel Rishak

- 4210181019 Sulthan Mahendra Mubarak

- 4210181020 Ilham Pratama

## Screenshot

![](Screenshot/Capture.PNG)

![](Screenshot/Capture1.PNG)

![](Screenshot/Capture2.PNG)

![](Screenshot/Capture3.PNG)

![](Screenshot/Capture4.PNG)

![](Screenshot/Capture5.PNG)

![](Screenshot/Capture6.PNG)

![](Screenshot/Capture7.PNG)
